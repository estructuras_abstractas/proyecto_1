#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 20/02/2020
 * 
 * @file nodo.h
 * @brief Se especifícan los métodos y atributos de la clase nodo.
 */ 

/**
 * @class nodo
 * @brief Se declara la estructura de la clase nodo.
 */
template <typename DATUM>
class nodo
{
    public:

        /**
         * @brief Constructor, inicia el nodo con un dato, los hijos derecho e izquierdo apuntan a null.
         * @param Info Información que se almacenará en el nodo.
         */
        nodo ( DATUM informacion );

        
        /**
         * @brief Regresa el dato que está almacenado en el nodo.
         * @return El dato que está almacenado en el nodo.
         */
        DATUM get_informacion ();

        /**
         * @brief Regresa el hijo izquierdo del nodo.
         * @return El hijo izquierdo del nodo.
         */
        nodo* get_hijo_izquierdo ();

        /**
         * @brief Regresa el hijo derecho del nodo.
         * @return El hijo derecho del nodo.
         */
        nodo* get_hijo_derecho ();

        /**
         * @brief Cambia la información que está almacenada en el nodo.
         * @param info La información que se almacenará.
         */
        void set_informacion ( DATUM informacion );

        /**
         * @brief Cambia el puntero del hijo izquierdo del nodo.
         * @param siguiente_nodo El nuevo nodo al que apuntará el nodo izquierdo.
         */
        void set_hijo_izquierdo ( nodo* izquierdo );

        /**
         * @brief Cambia el puntero del hijo derecho del nodo.
         * @param siguiente_nodo El nuevo nodo al que apuntará el hijo derecho.
         */
        void set_hijo_derecho ( nodo* derecho );


    private:

        DATUM info;//< Dato que contiene el nodo.

        nodo <DATUM>* hijo_izquierdo;//< Puntero al hijo izquierdo del nodo.
        nodo <DATUM>* hijo_derecho;//< Puntero al hijo derecho del nodo.
};

template class nodo <int>;