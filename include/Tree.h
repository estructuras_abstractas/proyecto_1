#pragma once 

/**
 * @author Jorge Muñoz Taylor
 * @date 20/02/2020
 * 
 * @file Tree.h
 * @brief Se especifícan los métodos y atributos de la clase Tree.
 */ 

#include "nodo.h"

/**
 * @class Tree
 * @brief Se declara la estructura de la clase Tree.
 */
template <typename DATUM>
class Tree
{
    public:

        /**
         * @brief Constructor, inicia el nodo raíz a null.
         */
        Tree ();

        /**
         * @brief Ingresa un nuevo nodo al árbol en caso de que su valor no esté en el árbol.
         * @param dato El dato que se ingresará.
         */
        void insert ( DATUM dato );
        
        /**
         * @brief Elimina un nodo del árbol.
         * @param dato El nodo que se eliminará.
         */
        void remove ( DATUM dato );

        /**
         * @brief Indica si el valor dado existe en el árbol o no.
         * @param dato Dato que se buscará.
         * @return True si el dato existe, false en caso contrario.
         */
        bool find ( DATUM dato );

        /**
         * @brief Regresa el nodo más pequeño a la izquierda.
         * @param encontrar_nodo Nodo de inicio de la búsqueda.
         * @return El nodo más pequeño a la izquierda.
         */
        nodo <DATUM>* findSmallestToTheRight ( nodo <DATUM>* encontrar_nodo );

        /**
         * @brief Imprime el árbol en preorden.
         */
        void preOrden ();

        /**
         * @brief Imprime el ábol en inorden.
         */
        void inOrden ();

        /**
         * @brief Imprime el árbol en posorden.
         */
        void posOrden ();

        /**
         * @brief Regresa el nodo raíz.
         * @return El nodo raíz.
         */
        nodo <DATUM>* get_raiz ();

    private:

        nodo <DATUM>* nodo_raiz;

        /**
         * @brief Imprime el árbol en preorden.
         * @param nodo_actual Recibe el nodo raíz del árbol.
         */
        void preOrden ( nodo<DATUM>* nodo_actual );

        /**
         * @brief Imprime el árbol en inorden.
         * @param nodo_actual Recibe el nodo raíz del árbol.
         */
        void inOrden ( nodo<DATUM>* nodo_actual );

        /**
         * @brief Imprime el árbol en posorden.
         * @param nodo_actual Recibe el nodo raíz del árbol.
         */
        void posOrden ( nodo<DATUM>* nodo_actual );

        /**
         * @brief Elimina un nodo de la estructura.
         * @param dato Dato que se usará para eliminar el nodo.
         * @param encontrar_nodo Nodo de inicio para la búsqueda.
         * @return El nodo que sustituirá al nodo que se eliminará.
         */
        nodo <DATUM>* remove ( DATUM dato, nodo <DATUM>* encontrar_nodo );
};

template class Tree <int>;