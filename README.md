# Proyecto 1: Árbol de búsqueda binario en C++


## Integrante
```
Jorge Munoz Taylor
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/proyecto1
```

Por último ejecute el make:
```
>>make
```

## Ejecutar el programa
Simplemente:
```
./bin/proyecto1
```

## Ejecutar las pruebas
```
./bin/test
```


## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/proyecto1
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias

Debe tener instalado CMAKE y MAKE en la máquina :
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```

También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```