ALL: packages comp doxygen test

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 0.5s

	sudo apt install g++ -y
	sudo apt-get install build-essential -y
	sudo apt install doxygen -y
	sudo apt install doxygen-gui -y
	sudo apt-get install graphviz -y
	sudo apt install texlive-latex-extra -y
	sudo apt install texlive-lang-spanish -y
	sudo apt-get install libsfml-dev -y
	sudo apt autoremove -y

	@echo " "
	@echo "-> Paquetes instalados"


comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 0.5s

	mkdir -p bin build docs

	g++ -o ./build/nodo.o -c ./src/nodo.cpp

	g++ -o ./build/Tree.o -c ./src/Tree.cpp 

	g++ -o ./build/main.o -c ./src/main.cpp

	g++ -o ./bin/proyecto1 ./build/nodo.o ./build/Tree.o ./build/main.o 

	@echo " "
	@echo "-> Ejecutable generado"

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 0.5s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex

	@echo " "
	@echo "-> Archivo refman.pdf generado  "


test:
	@echo "**************************************************************************"
	@echo "************             Test para el BST                  ***************"
	@echo "**************************************************************************"
	
	@g++ -o ./build/nodo.o -c ./src/nodo.cpp
	@g++ -o ./build/Tree.o -c ./src/Tree.cpp 
	@g++ -o ./build/test.o -c ./src/test.cpp

	@g++ -o ./bin/test ./build/nodo.o ./build/Tree.o ./build/test.o 
	
	./bin/test

	@echo " "
	@echo "-> Finalizó el test"