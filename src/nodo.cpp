/**
 * @author Jorge Muñoz Taylor
 * @date 20/02/2020
 * 
 * @file nodo.cpp
 * @brief Se definen los métodos de la clase nodo.
 */ 

#include <iostream>
#include "../include/nodo.h"

using namespace std;


template <typename DATUM> 
nodo<DATUM>::nodo ( DATUM informacion)
{
    this->info           = informacion;
    this->hijo_izquierdo = nullptr;
    this->hijo_derecho   = nullptr;
}



template <typename DATUM> 
DATUM nodo<DATUM>::get_informacion ()
{  
    return this->info;
}



template <typename DATUM> 
nodo<DATUM>* nodo<DATUM>::get_hijo_izquierdo()
{
    return this->hijo_izquierdo;
}



template <typename DATUM> 
nodo<DATUM>* nodo<DATUM>::get_hijo_derecho ()
{
    return this->hijo_derecho;
}


template <typename DATUM> 
void nodo<DATUM>::set_informacion ( DATUM informacion )
{
    this->info = informacion;
}



template <typename DATUM> 
void nodo<DATUM>::set_hijo_izquierdo ( nodo* izquierdo )
{
    this->hijo_izquierdo = izquierdo;
}


template <typename DATUM> 
void nodo<DATUM>::set_hijo_derecho ( nodo* derecho )
{
    this->hijo_derecho = derecho;
}