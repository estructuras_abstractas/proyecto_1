/**
 * @author Jorge Muñoz Taylor
 * @date 20/02/2020
 * 
 * @file test.cpp
 * @brief Código que permite probar el árbol binario Tree.
 */ 

#include <iostream>
#include <stdio_ext.h>
#include "../include/Tree.h"

#define PROGRAMA_TERMINO_BIEN 0

using namespace std;

int main ()
{
    Tree<int> arbol;
   
    //El nodo raíz será 50.
    arbol.insert(50);
    arbol.insert(20);
    arbol.insert(10);
    arbol.insert(21);
    arbol.insert(3);
    arbol.insert(4);
    arbol.insert(100);
    arbol.insert(90);
    arbol.insert(101);
    arbol.insert(200);
    arbol.insert(80);

    cout << endl;
    arbol.inOrden();

    cout << endl;
    arbol.preOrden();

    cout << endl;
    arbol.posOrden();

    cout << endl << endl << endl;
    cout << "-> Se buscará el valor 10:" << endl;
    arbol.find(10);

    cout << "-> Se buscará el valor 21:" << endl;
    arbol.find(21);

    cout << "-> Se buscará el valor 90:" << endl;
    arbol.find(90);

    cout << "-> Se eliminará el valor 100:" << endl;
    arbol.remove (100);
    arbol.inOrden();

    cout << endl << endl;
    cout << "-> Se eliminará el valor 3:" << endl;
    arbol.remove (3);
    arbol.inOrden();

    cout << endl << endl;


    return PROGRAMA_TERMINO_BIEN;
}