/**
 * @author Jorge Muñoz Taylor
 * @date 20/02/2020
 * 
 * @file main.cpp
 * @brief Mantiene la lógica principal del prograna.
 */ 

#include <iostream>
#include <stdio_ext.h>
#include "../include/Tree.h"

#define PROGRAMA_TERMINO_BIEN 0
#define INSERTAR 1
#define REMOVER 2
#define ENCONTRAR 3
#define PREORDEN 4
#define INORDEN 5
#define POSORDEN 6
#define SALIR 7

using namespace std;

int main ()
{
    Tree<int> arbol;
    
    bool continuar = true;
    bool lazo = true;
    int opcion;
    int dato;

    
    while ( continuar == true )
    {
        cin.clear();
        __fpurge(stdin);

        cout << endl;
        cout << "1. insertar" << endl;
        cout << "2. remover"  << endl;
        cout << "3. encontrar"<< endl;
        cout << "4. preOrden" << endl;
        cout << "5. inOrden"  << endl;
        cout << "6. posOrden" << endl;
        cout << "7. salir"    << endl << endl;

        cin >> opcion;
    
        switch (opcion)
        {
            case INSERTAR:

                lazo = true;
                while ( lazo == true )
                {
                    cin.clear();
                    __fpurge(stdin);

                    cout << endl << "--> Dato: ";
                    cin >> dato;

                    if ( !cin )
                        cout << endl << "=> Escriba un número :V" << endl << endl;
                    
                    else 
                        lazo = false;
                    
                }

                arbol.insert( dato );
                break;
        

            case REMOVER:

                lazo = true;
                while ( lazo == true )
                {
                    cin.clear();
                    __fpurge(stdin);

                    cout << "Dato: ";
                    cin >> dato;

                    if ( !cin )
                        cout << endl << "=> Escriba un número :V" << endl;
                    
                    else 
                        lazo = false;
                }

                arbol.remove ( dato );
                break;


            case ENCONTRAR:
                lazo = true;
                while ( lazo == true )
                {
                    cin.clear();
                    __fpurge(stdin);

                    cout << "Dato: ";
                    cin >> dato;

                    if ( !cin )
                        cout << endl << "=> Escriba un número :V" << endl;
                    
                    else 
                        lazo = false;
                }

                arbol.find( dato );
                break;


            case PREORDEN:

                cout << endl;
                arbol.preOrden();
                cout << endl;
                break;


            case INORDEN:

                cout << endl;
                arbol.inOrden();
                cout << endl;
                break;


            case POSORDEN:

                cout << endl;
                arbol.posOrden();
                cout << endl;
                break;


            case SALIR:
                continuar = false;
                break;


            default:
                cout << endl << "-> Esa opción no existe >_o" << endl;
                break;
        }//Fin de switch

    }//Fin de while


    cout << endl << "~~ Chau ;o ~~" << endl << endl;

    return PROGRAMA_TERMINO_BIEN;
}