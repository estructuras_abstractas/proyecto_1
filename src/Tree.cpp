/**
 * @author Jorge Muñoz Taylor
 * @date 20/02/2020
 * 
 * @file Tree.cpp
 * @brief Se definen los métodos de la clase Tree.
 */ 

#include <iostream>
#include "../include/Tree.h"

using namespace std;


template <typename DATUM>
Tree<DATUM>::Tree ()
{
    this->nodo_raiz = nullptr;
}


template <typename DATUM>
void Tree<DATUM>::insert ( DATUM dato )
{
    nodo <DATUM>* nodo_actual = this->nodo_raiz;

    if (this->nodo_raiz == nullptr )
    {
        this->nodo_raiz = new nodo<DATUM> (dato);
        return;
    }


    while (true)
    {
        if (dato < nodo_actual->get_informacion())
        {
            if (nodo_actual->get_hijo_izquierdo() == NULL)
            {
                nodo <DATUM>* nuevo_nodo = new nodo <DATUM> (dato);
                nodo_actual->set_hijo_izquierdo (nuevo_nodo);
                break;
            }
            else
            {
                nodo_actual = nodo_actual->get_hijo_izquierdo();
            }
        }
            
        else if (dato > nodo_actual->get_informacion())
        {
            if (nodo_actual->get_hijo_derecho() == NULL)
            {
                nodo <DATUM>* nuevo_nodo = new nodo <DATUM> (dato);
                nodo_actual->set_hijo_derecho (nuevo_nodo);
                break;
            }
            else
            {
                nodo_actual = nodo_actual->get_hijo_derecho();
            }
        }

        else return;//Si el dato y el nodo son iguales no hace nada.

    }//Fin de while

}


template <typename DATUM>
void Tree<DATUM>::remove ( DATUM dato )
{
    nodo <DATUM>* nodo_actual = this->nodo_raiz;
    
    if (this->nodo_raiz == nullptr )
    {
        cout << endl << "-> El arbolito está vacío :3" << endl << endl;
        return;
    }

    if ( this->nodo_raiz->get_hijo_izquierdo() == nullptr &&
         this->nodo_raiz->get_hijo_derecho()   == nullptr &&
         this->nodo_raiz->get_informacion() == dato )
    {
        this->nodo_raiz = nullptr;
        return;
    }

    if ( this->nodo_raiz->get_informacion() == dato )
    {
        this->nodo_raiz = this->findSmallestToTheRight(this->nodo_raiz->get_hijo_derecho());
        return;
    }

    this->remove ( dato, nodo_actual );
}


template <typename DATUM>
nodo <DATUM>* Tree<DATUM>::remove ( DATUM dato, nodo <DATUM>* encontrar_nodo )
{    
    if (encontrar_nodo == nullptr) return encontrar_nodo; 
  
   
    if (dato < encontrar_nodo->get_informacion() ) 
        encontrar_nodo->set_hijo_izquierdo ( remove ( dato, encontrar_nodo->get_hijo_izquierdo() ) ); 
  
 
    else if (dato > encontrar_nodo->get_informacion() ) 
        encontrar_nodo->set_hijo_derecho ( remove ( dato, encontrar_nodo->get_hijo_derecho() ) );  
  
  
    else
    { 
        nodo <DATUM>* temp;

        if (encontrar_nodo->get_hijo_izquierdo() == nullptr) 
        { 
            temp = encontrar_nodo->get_hijo_derecho(); 
            delete (encontrar_nodo); 
            encontrar_nodo = nullptr;
            return temp; 
        } 
        else if (encontrar_nodo->get_hijo_derecho() == nullptr) 
        { 
            temp = encontrar_nodo->get_hijo_izquierdo(); 
            delete (encontrar_nodo); 
            encontrar_nodo = nullptr;
            return temp; 
        } 
  
        temp = findSmallestToTheRight (encontrar_nodo->get_hijo_derecho()); 
  
        encontrar_nodo->set_informacion ( temp->get_informacion() ); 
  
        encontrar_nodo->set_hijo_derecho( remove ( temp->get_informacion(), encontrar_nodo->get_hijo_derecho() ) ); 
    
    } 

    return encontrar_nodo; 
}


template <typename DATUM>
bool Tree<DATUM>::find ( DATUM dato )
{
    nodo <DATUM>* nodo_actual = this->nodo_raiz;


    if (this->nodo_raiz == nullptr )
    {
        cout << endl << "-> El arbolito está vacío :3" << endl << endl;
        return false;
    }


    while ( nodo_actual != nullptr )
    {
        if (dato < nodo_actual->get_informacion())
        {
            nodo_actual = nodo_actual->get_hijo_izquierdo();
        }
            
        else if (dato > nodo_actual->get_informacion())
        {
            nodo_actual = nodo_actual->get_hijo_derecho();
        }

        else if ( dato == nodo_actual->get_informacion() )//Si el dato y el nodo son iguales no hace nada.
        {
            cout << endl << "--> El dato " << dato << " si está en el árbol :D" << endl << endl;
            return true;
        }
    }//Fin de while

    cout << endl << "--> El dato " << dato << " no está en el árbol :(" << endl << endl;
    return false;
}


template <typename DATUM>
nodo <DATUM>* Tree<DATUM>::findSmallestToTheRight ( nodo <DATUM>* encontrar_nodo )
{
    nodo <DATUM>* temp = encontrar_nodo;

    while ( temp && temp->get_hijo_izquierdo() != nullptr )
    {
        if ( temp->get_hijo_izquierdo() != nullptr )
        {
            temp = temp->get_hijo_izquierdo();
        }
    }//Fin de while

    return temp;
}


template <typename DATUM>
void Tree<DATUM>::preOrden ()
{
    nodo <DATUM>* nodo_actual = this->nodo_raiz;
    
    if (this->nodo_raiz == nullptr )
    {
        cout << endl << "-> El arbolito está vacío :3" << endl << endl;
        return;
    }

    cout << "   Preorden: [ ";
    this->preOrden ( nodo_actual );
    cout << "]";
}


template <typename DATUM>
void Tree<DATUM>::preOrden ( nodo<DATUM>* nodo_actual )
{
	if(nodo_actual == nullptr) return;

	cout << nodo_actual->get_informacion() <<" ";
	this->preOrden(nodo_actual->get_hijo_izquierdo());   
	this->preOrden(nodo_actual->get_hijo_derecho());   
}


template <typename DATUM>
void Tree<DATUM>::inOrden ()
{
    nodo <DATUM>* nodo_actual = this->nodo_raiz;
    
    if (this->nodo_raiz == nullptr )
    {
        cout << endl << "-> El arbolito está vacío :3" << endl << endl;
        return;
    }

    cout << "   Inorden: [ ";
    this->inOrden ( nodo_actual );
    cout << "]";
       
}


template <typename DATUM>
void Tree<DATUM>::inOrden ( nodo<DATUM>* nodo_actual ) 
{
   if (nodo_actual != nullptr) 
   {   
      this->inOrden (nodo_actual->get_hijo_izquierdo() );
   
      cout << nodo_actual->get_informacion() <<" ";
   
      this->inOrden (nodo_actual->get_hijo_derecho() );
   }
} 


template <typename DATUM>
void Tree<DATUM>::posOrden ()
{
    nodo <DATUM>* nodo_actual = this->nodo_raiz;
    
    if (this->nodo_raiz == nullptr )
    {
        cout << endl << "-> El arbolito está vacío :3" << endl << endl;
        return;
    }

    cout << "   Posorden: [ ";
    this->posOrden ( nodo_actual );
    cout << "]";
}


template <typename DATUM>
void Tree<DATUM>::posOrden ( nodo<DATUM>* nodo_actual )
{
	if(nodo_actual == nullptr) return;

	this->posOrden (nodo_actual->get_hijo_izquierdo());  
	this->posOrden (nodo_actual->get_hijo_derecho());   
	cout << nodo_actual->get_informacion() <<" ";
}


template <typename DATUM>
nodo <DATUM>* Tree<DATUM>::get_raiz ()
{
    return this->nodo_raiz;
}